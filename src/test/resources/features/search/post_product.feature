Feature: Search for the product
Scenario: Cola Product Verification
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/cola"
    Then he sees the results displayed for 'cola'

Scenario: Apple Product Verification
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the results displayed for 'apple'
    
Scenario: Pasta Product Verification    
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/pasta"
    Then he sees the results displayed for 'pasta'
    
Scenario: Mango Product Verification
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/mango"
    Then he doesn not see the results    

Scenario: Cars Product Verification 
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/car"
    Then he doesn not see the results
