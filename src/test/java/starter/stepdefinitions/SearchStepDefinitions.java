package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.assertj.core.api.Assertions;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

public class SearchStepDefinitions {

    private Response response;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String url) {
        response = SerenityRest.given().get(url);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedFor(String productName) {
        List<Map<String, Object>> jsonResponses = response.jsonPath().getList("$");

        for (Map<String, Object> jsonResponse : jsonResponses) {
            String title = ((String) jsonResponse.get("title")).toLowerCase();
            Assertions.assertThat(title).containsIgnoringCase(productName.toLowerCase());
        }
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        Boolean errorValue = response.getBody().jsonPath().get("detail.error");
        Assertions.assertThat(errorValue).isTrue();
    }
}
